# 裝飾模式(範例2)

Java API中，<b>java.io<b>就是使用裝飾模式

這也意味著我們可以自行設計出符合我們功能需求的裝飾元件
e.g.
```java
public class ReverseReader extends BufferedReader {

	public ReverseReader(Reader in) {
		super(in);
	}

	public String reverseLine() throws IOException {
		String line = super.readLine();
		if (null == line) {
			return null;
		}
		return reverse(line);
	}

	private String reverse(String source) {
		String result = "";
		for (int i = 0; i < source.length(); i++) {
			result = source.charAt(i) + result;
		}
		return result;
	}
}
```
此類別並未改變Component的功能(FileReader.readLine())
然而卻可以實現句子反轉的功能。