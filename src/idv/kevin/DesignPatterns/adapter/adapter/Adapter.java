package idv.kevin.DesignPatterns.adapter.adapter;

import idv.kevin.DesignPatterns.adapter.source.Archer;
import idv.kevin.DesignPatterns.adapter.target.Wizard;

public class Adapter implements Wizard {
	private Archer archer;
	
	public Adapter(Archer archer) {
		this.archer = archer;
	}

	@Override
	public void fireBall() {
		System.out.println("弓箭上包一層布->淋油->點火");
		archer.shot();
		System.out.println("射出火球");
	}

}
