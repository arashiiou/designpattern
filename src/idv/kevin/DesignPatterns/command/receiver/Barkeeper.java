package idv.kevin.DesignPatterns.command.receiver;

public class Barkeeper implements KitchenWorker {

	@Override
	public void finishWork() {
		System.out.print("do Barkeeper's job ");
		System.out.println();
	}
}
