# 觀察者模式

在觀察者模式中，主要存在兩種角色，“觀察者(Observer)”、“主題(Subject)(又稱被觀察者Observable)”<br>
是一種一對多的依賴關係，當主題(subject)的狀態有所改變，所有依賴他的觀察者(subject)都會被通知，<br>
且各自做出反應(update)

<img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Observer_w_update.svg" height="250">


此模式極像是使用者於Youtube訂閱特定頻道，當頻道有所更新時，訂閱者都會被通知。