package idv.kevin.DesignPatterns.template.template;

public class EasyMaze extends MazeTemplate {
	public EasyMaze() {
		super.difficulty = 1;
	}

	@Override
	void createMaze() {
		System.out.println("level:1 easy maze created!!");
		System.out.println("the maze only 10x10 size");
	}

	@Override
	void start() {
		System.out.println("挑戰者進行簡單迷宮～");
	}

}
