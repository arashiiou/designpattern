package idv.kevin.DesignPatterns.builder.builder;

public class Mac {
	private String model;
	private Double monitorSize;
	private String color;
	private Integer storage;
	private Integer memory;
	private String processor;
	private String keyboard;

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getMonitorSize() {
		return monitorSize;
	}

	public void setMonitorSize(Double monitorSize) {
		this.monitorSize = monitorSize;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getStorage() {
		return storage;
	}

	public void setStorage(Integer storage) {
		this.storage = storage;
	}

	public Integer getMemory() {
		return memory;
	}

	public void setMemory(Integer memory) {
		this.memory = memory;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getKeyboard() {
		return keyboard;
	}

	public void setKeyboard(String keyboard) {
		this.keyboard = keyboard;
	}
}
