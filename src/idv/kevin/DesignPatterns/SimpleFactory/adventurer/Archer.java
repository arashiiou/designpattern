package idv.kevin.DesignPatterns.simpleFactory.adventurer;

public class Archer implements Adventurer {

	@Override
	public String getType() {
		System.out.println("I am archer");
		return this.getClass().getSimpleName();
	}

}
