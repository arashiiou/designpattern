# 策略模式

<img src="https://upload.wikimedia.org/wikipedia/commons/3/39/Strategy_Pattern_in_UML.png" width="350">
<br><br>

情境：存在一堆櫻桃(List<Cherry> cherries)，依不同的條件篩選出符合條件的櫻桃，<br>
這邊所提到的 "篩選條件" 即為 策略(Strategy)。

不使用策略模式之前，<br>
程式碼中充滿了 filterxxxx(cherries)的方法<br>
e.g.
``` 
filterRedCherries(cherries) or 
filterWhiteCherries(cherries) or 
filterBigCherries(cherries)...
``` 

使用策略模式，<br>
先定義一個strategy interface，並規範出一個慾執行的方法，<br>
再讓建立多個不同的策略類別並實作strategy interface，

接下來我們只需要一個主要的filter方法中，傳入apples與所要執行的策略,
即可得到該執行該策略後我們期望獲得的結果

```
filterByStrategy(cherries, strategy)
```
