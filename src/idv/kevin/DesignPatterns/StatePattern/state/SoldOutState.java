package idv.kevin.DesignPatterns.statePattern.state;

import idv.kevin.DesignPatterns.statePattern.GumballMachine;

public class SoldOutState implements State {
	GumballMachine gumballMachine;

	public SoldOutState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void inserQuarter() {
		System.out.println("Out of gumballs");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("No quarter can't be refund");
		System.out.println("Out of gumballs");
	}

	@Override
	public void turnCrank() {
		System.out.println("Can't turn the crank");
		System.out.println("Out of gumballs");
	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispense");
	}

}
