package idv.kevin.DesignPatterns.command.command;

import idv.kevin.DesignPatterns.command.receiver.KitchenWorker;

public abstract class Order {
	protected KitchenWorker receiver;
	protected String name;

	public Order(KitchenWorker receiver) {
		this.receiver = receiver;
	}

	public void sendOrder() {
		receiver.finishWork();
	}

	public String getName() {
		return this.name;
	}
}
