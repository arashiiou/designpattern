package idv.kevin.DesignPatterns.facade.electronics;

public abstract class Electronic {
	private boolean power = false;

	public void powerOn() {
		this.power = true;
	}

	public void powerOff() {
		this.power = false;
	}

	public boolean isPowerOn() {
		return this.power;
	}

	public void showStatus() {
		if (power) {
			System.out.println(this.getClass().getSimpleName()
					+ " power on and running...");
		} else {
			System.out
					.println(this.getClass().getSimpleName() + " power off...");
		}
	}
}
