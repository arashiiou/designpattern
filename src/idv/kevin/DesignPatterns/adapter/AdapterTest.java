package idv.kevin.DesignPatterns.adapter;

import org.junit.Test;

import idv.kevin.DesignPatterns.adapter.adapter.Adapter;
import idv.kevin.DesignPatterns.adapter.source.NormalArcher;

public class AdapterTest {

	@Test
	public void test() {
		System.out.println("====未變身前====");
		NormalArcher normalArcher = new NormalArcher();
		normalArcher.shot();

		System.out.println("=====變身後=====");
		Adapter otherTypeArcher = new Adapter(normalArcher);
		otherTypeArcher.fireBall();
	}

}
