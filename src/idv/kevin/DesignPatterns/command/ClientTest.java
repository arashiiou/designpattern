package idv.kevin.DesignPatterns.command;

import org.junit.Test;

import idv.kevin.DesignPatterns.command.command.DrinkOrder;
import idv.kevin.DesignPatterns.command.command.FoodOrder;
import idv.kevin.DesignPatterns.command.invoker.Waitress;
import idv.kevin.DesignPatterns.command.receiver.Barkeeper;
import idv.kevin.DesignPatterns.command.receiver.Chef;

public class ClientTest {

	@Test
	public void tset() {
		FoodOrder foodOrder = new FoodOrder(new Chef());
		DrinkOrder drinkOrder = new DrinkOrder(new Barkeeper());

		Waitress waitress = new Waitress();

		System.out.println("=====客人點餐====");
		waitress.setOrder(foodOrder);
		waitress.setOrder(foodOrder);
		waitress.setOrder(drinkOrder);
		waitress.setOrder(drinkOrder);
		waitress.setOrder(drinkOrder);

		System.out.println("=====取消又重新點餐====");

		waitress.cancelOrder(foodOrder);
		waitress.setOrder(foodOrder);

		System.out.println("=====將點餐單送至廚房====");
		waitress.sendOrderToKitchen();

		System.out.println("=====食物不足時點餐====");
		waitress.setOrder(foodOrder);
	}
}
