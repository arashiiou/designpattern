package idv.kevin.DesignPatterns.observer.subject;

import java.util.ArrayList;
import java.util.List;

import idv.kevin.DesignPatterns.observer.observer.Adventurer;

public abstract class Subject {

	List<Adventurer> observers = new ArrayList<>();

	public void add(Adventurer observer) {
		observers.add(observer);
	}

	public void remove(Adventurer observer) {
		observers.remove(observer);
	}

	public abstract void sendQuestions(String questions);
}
