# 建造模式

<img src="https://upload.wikimedia.org/wikipedia/commons/f/f3/Builder_UML_class_diagram.svg" height="300">

在完整造模式中，共有四種角色
- Product: 最後實體畫出來的物件類別
- Builder: 定義物件構建過程中的所有方法的介面(或抽象類別)
- ConcreteBuilder: 實作Builder，實際用以構建物件的類別
- Director: 下達構建指令給ConcreteBuilder(實際使用Builder的物件)
於此範例省略了 Director

JAVA API中 StringBuilder即為 建造模式
