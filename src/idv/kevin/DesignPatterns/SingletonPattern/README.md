# 單例模式

單例模式的主要精神在於：<br>
整個系統種只存在一個實例，<br>
最常出現的做法是，類別本身的建構式為private
且提供一個public getInstance的方法來取得此一唯一的物件
