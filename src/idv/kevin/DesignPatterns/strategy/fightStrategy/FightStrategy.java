package idv.kevin.DesignPatterns.strategy.fightStrategy;

public interface FightStrategy {

	void execute();
}
