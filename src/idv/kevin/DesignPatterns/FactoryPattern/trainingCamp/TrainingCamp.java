package idv.kevin.DesignPatterns.factoryPattern.trainingCamp;

import idv.kevin.DesignPatterns.factoryPattern.adventuter.Adventurer;

public interface TrainingCamp {
	public Adventurer trainAdventurer();
}
