# 命令模式

<img src="https://s3.notfalse.net/wp-content/uploads/2016/12/24154946/command-pattern-class-diagram1.png" height="300"/>

命令模式的精神在於，將指令的建立與執行分離<br>
為此會有三種角色存在，
- Command: interface or abstract class，必定有一個execute方法
           負責封裝指令。
- Invoker: 負責儲存與呼叫命令。
- Receiver: 負責執行命令的內容。
