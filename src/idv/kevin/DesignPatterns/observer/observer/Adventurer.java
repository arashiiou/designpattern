package idv.kevin.DesignPatterns.observer.observer;

import idv.kevin.DesignPatterns.observer.subject.Subject;

public abstract class Adventurer {
	protected String name;
	protected Subject subject;

	public Adventurer(String name) {
		this.name = name;
	}
	
	public void listen(Subject subject) {
		this.subject = subject;
		subject.add(this);
	};
	
	public void unListen(Subject subject) {
		this.subject = subject;
		subject.remove(this);
	};

	public abstract void getQuestions(String questions);

}
