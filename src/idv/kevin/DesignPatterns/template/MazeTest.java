package idv.kevin.DesignPatterns.template;

import org.junit.Test;

import idv.kevin.DesignPatterns.template.template.HardMaze;
import idv.kevin.DesignPatterns.template.template.EasyMaze;
import idv.kevin.DesignPatterns.template.template.MazeTemplate;

public class MazeTest {
	Adventurer novice = new Novice();
	Adventurer knight = new Knight();

	MazeTemplate easyMaze = new EasyMaze();
	MazeTemplate hardMaze = new HardMaze();

	@Test
	public void test() {
		System.out.println("困難迷宮");
		novice = hardMaze.adventurer(novice);
		System.out.println();
		System.out.println("簡單迷宮");
		novice = easyMaze.adventurer(novice);
		System.out.println();
		System.out.println("再次困難迷宮");
		novice = hardMaze.adventurer(novice);
		System.out.println();
		
		System.out.println("====換人囉====");
		System.out.println("困難迷宮");
		novice = hardMaze.adventurer(novice);
		System.out.println();
		System.out.println("簡單迷宮");
		novice = easyMaze.adventurer(novice);
		System.out.println();
		
	
		
	}
}
