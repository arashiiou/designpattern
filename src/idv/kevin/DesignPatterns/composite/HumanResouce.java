package idv.kevin.DesignPatterns.composite;

public class HumanResouce extends Department {

	public HumanResouce(String name) {
		super(name);
	}

	@Override
	public void listDuty() {
		System.out.println(name+": 招募人力");
	}

}
