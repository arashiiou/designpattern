package idv.kevin.DesignPatterns.strategy2.comparator;

import java.util.Comparator;

import idv.kevin.DesignPatterns.strategy2.Village;

public class SortVillageByArea implements Comparator<Village> {

	@Override
	public int compare(Village o1, Village o2) {
		if (o1.area > o2.area)
			return 1;

		if (o1.area < o2.area)
			return -1;
		return 0;
	}
}
