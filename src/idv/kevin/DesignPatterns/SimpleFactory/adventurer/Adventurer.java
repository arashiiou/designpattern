package idv.kevin.DesignPatterns.simpleFactory.adventurer;

public interface Adventurer {
	String getType();
}
