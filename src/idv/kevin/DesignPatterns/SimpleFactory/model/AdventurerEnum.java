package idv.kevin.DesignPatterns.simpleFactory.model;

public enum AdventurerEnum {
	ARCHER("archer"),
	WARRIOR("warrior");

	private String adventureType;

	private AdventurerEnum(String adventureType) {
		this.adventureType = adventureType;
	}

	public String getAdventureType() {
		return adventureType;
	}
}
