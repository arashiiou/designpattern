package idv.kevin.DesignPatterns.facade.electronics;

import idv.kevin.DesignPatterns.facade.source.SourceEnum;

public class Television extends Electronic {
	private int sound = 50;
	private SourceEnum source = SourceEnum.TVBOX;
	private int channel = 9;

	public void switchSource(SourceEnum source) {
		this.source = source;
	}

	public void setSound(int sound) {
		this.sound = sound;
	}

	public void swithcChannel(int channel) {
		this.channel = channel;
	}

	public void showTvChannel() {
		System.out.println("現正播放第 " + channel + " 台");
	}

	public int getSound() {
		return sound;
	}

	public SourceEnum getSource() {
		return source;
	}

	public int getChannel() {
		return channel;
	}

	@Override
	public void showStatus() {
		super.showStatus();
		if (isPowerOn()) {
			switch (source) {
				case TVBOX :{
					System.out.print("頻道為: " + channel);
					break;
				}
				case KTV :{
					System.out.print("KTV 播放中");
					break;
				}
				case PS :{
					System.out.print("PS遊戲機 播放中");
					break;
				}
				default:{}
			}
			System.out.println("  音量為:" + sound);
		}
	}
}
