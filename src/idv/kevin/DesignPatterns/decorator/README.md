# 裝飾模式

<img src="https://upload.wikimedia.org/wikipedia/commons/e/e9/Decorator_UML_class_diagram.svg" width="350">

裝飾者模式有兩個主要的角色
- Component：被裝飾的核心元件
- Decorator：裝飾核心的其他元件

其應用場景在於當被裝飾者(component)需要增加額外的功能，卻又不想改變現有程式碼的應用


假設有一元件類別(或是介面)Adventurer 本身只有一個 attack()，

```java
public interface Adventurer {
	void attack();
}
```

因需求必須增加 useSkill()的方法時，若不使用*裝飾模式*

```java
public interface Adventurer {
	void attack();
    void useSkill();
}
```
影響到的是其餘跟Adventurer有耦合關係之類別與介面

當我們使用裝飾模式時，首先先建立一個Decorator並將Component作為屬性
```java
public abstract class Title implements Adventurer {

	protected Adventurer adventurer;

	public Title(Adventurer adventurer) {
		this.adventurer = adventurer;
	}

	@Override
	public void attack() {
		adventurer.attack();
	}
}
```

而且只需要依據需求增加Component的實作類便可達到擴充類別的需求
```java
public class TitleAgile extends Title {

	public TitleAgile(Adventurer adventurer) {
		super(adventurer);
	}

	@Override
	public void attack() {
		System.out.print("快速 ");
		super.attack();
	}

	public void useFlash() {
		System.out.println("使用瞬間移動 ");
	}
}
```
