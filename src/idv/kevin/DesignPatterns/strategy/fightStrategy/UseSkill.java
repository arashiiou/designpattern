package idv.kevin.DesignPatterns.strategy.fightStrategy;

public class UseSkill implements FightStrategy {

	@Override
	public void execute() {
		System.out.println("use Skill attack");
	}
}
