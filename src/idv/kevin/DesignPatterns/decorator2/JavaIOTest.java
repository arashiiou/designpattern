package idv.kevin.DesignPatterns.decorator2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import idv.kevin.DesignPatterns.decorator2.decorator.ReverseReader;
import org.junit.Test;

public class JavaIOTest {

	@SuppressWarnings("resource")
	@Test
	public void test() throws IOException {
		File file = new File("src/idv/kevin/DesignPatterns/decorator2/test.txt");

		System.out.println("======FileReader讀取檔案======");
		FileReader fr = new FileReader(file);
		int c = fr.read();
		while (c >= 0) {
			System.out.print((char) c);
			c = fr.read();
		}

		System.out.println();
		System.out.println("======BufferedReader讀取檔案======");
		BufferedReader br = new BufferedReader(new FileReader(file));
		String line = br.readLine();
		while (null != line) {
			System.out.println(line);
			line = br.readLine();
		}

		ReverseReader rr = new ReverseReader(new FileReader(file));
		String reverseLine = rr.reverseLine();
		while (null != reverseLine) {
			System.out.println(reverseLine);
			reverseLine = rr.reverseLine();
		}
	}
}
