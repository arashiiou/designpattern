package idv.kevin.DesignPatterns.composite;
/*
 * 部門單位抽象類別 Leaf
*/
public abstract class Department extends AbstractAssociation {

	public Department(String name) {
		super(name);
	}

	@Override
	public void add(AbstractAssociation association) {
		System.out.println("Leaf無法增加子節點");
	}

	@Override
	public void remove(AbstractAssociation association) {
		System.out.println("Leaf無子節點可移除");
	}

	@Override
	public void display(int depth) {
		for (int i = 0; i < depth; i++) {
			System.out.print('-');
		}
		System.out.println(name);
	}

	@Override
	abstract public void listDuty();

}
