package idv.kevin.DesignPatterns.adapter.source;

public interface Archer {
	void shot();
}
