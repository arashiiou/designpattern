package idv.kevin.DesignPatterns.abstractFactory.adventurer;

import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Clothes;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Weapon;

public abstract class Adventurer {
	protected Weapon weapon;
	protected Clothes clothes;

	public abstract void disPlay();

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	public Clothes getClothes() {
		return clothes;
	}

	public void setClothes(Clothes clothes) {
		this.clothes = clothes;
	}
	
	
}
