package idv.kevin.DesignPatterns.factoryPattern;

import org.junit.Assert;
import org.junit.Test;

import idv.kevin.DesignPatterns.factoryPattern.adventuter.Adventurer;
import idv.kevin.DesignPatterns.factoryPattern.trainingCamp.ArcherTrainingCamp;
import idv.kevin.DesignPatterns.factoryPattern.trainingCamp.WarriorTrainingCamp;

public class TrainingCampTest {
	
	@Test
	public void test() {
		ArcherTrainingCamp archerTrainingCamp = new ArcherTrainingCamp();
		WarriorTrainingCamp warriorTrainingCamp = new WarriorTrainingCamp();
		
		Adventurer archer = archerTrainingCamp.trainAdventurer();
		Adventurer warrior = warriorTrainingCamp.trainAdventurer();
		
		Assert.assertEquals(archer.getType(), "Archer");
		Assert.assertEquals(warrior.getType(), "Warrior");
	}

}
