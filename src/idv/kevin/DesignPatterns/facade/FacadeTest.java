package idv.kevin.DesignPatterns.facade;

import org.junit.Test;

import idv.kevin.DesignPatterns.facade.livingRoom.LivingRoomFacade;

public class FacadeTest {
	LivingRoomFacade livingRooomRemote = new LivingRoomFacade();
	
	@Test
	public void test() {
		System.out.println("----- play game -----");
		livingRooomRemote.playGame("FF XV");
		livingRooomRemote.showAllStatus();

		System.out.println();
		System.out.println("----- turn off machine -----");
		livingRooomRemote.turnOffAll();
		livingRooomRemote.showAllStatus();

		System.out.println();
		System.out.println("----- watch tv -----");
		livingRooomRemote.watchTV();
		livingRooomRemote.showTVChannel();
		livingRooomRemote.switchTVChannel(99);
		livingRooomRemote.showTVChannel();
		livingRooomRemote.turnOffAll();
		

		System.out.println();
		System.out.println("----- sing karaoke -----");
		livingRooomRemote.useKTV();
		livingRooomRemote.selectSong("A Thousand Years");
		livingRooomRemote.playSong();
		livingRooomRemote.showAllStatus();
	}
}
