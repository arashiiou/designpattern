package idv.kevin.DesignPatterns.composite;

public class ServiceCenter extends Department {

	public ServiceCenter(String name) {
		super(name);
	}

	@Override
	public void listDuty() {
		System.out.println(name+": 處理各種問題");
	}

}
