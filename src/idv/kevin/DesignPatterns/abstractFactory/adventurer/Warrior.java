package idv.kevin.DesignPatterns.abstractFactory.adventurer;

public class Warrior extends Adventurer {

	@Override
	public void disPlay() {
		System.out.println("I am Warrior, and I have");
		weapon.display();
		clothes.display();
	}

}
