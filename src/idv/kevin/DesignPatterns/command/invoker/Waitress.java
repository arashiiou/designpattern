package idv.kevin.DesignPatterns.command.invoker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import idv.kevin.DesignPatterns.command.command.Order;

public class Waitress {
	private int foodQty = 2;
	private int drinkQty = 4;
	private List<Order> orderList = new LinkedList<>();


	public void setOrder(Order order) {

		if ("FoodOrder".equals(order.getName())) {
			if (foodQty < 1) {
				System.out.println("food sold out");
			} else {
				System.out.println("add food order");
				foodQty--;
				orderList.add(order);
			}
		} else if ("DrinkOrder".equals(order.getName())) {
			if (drinkQty < 1) {
				System.out.println("drick sold out");
			} else {
				System.out.println("add drink order");
				drinkQty--;
				orderList.add(order);
			}
		}
	}

	public void cancelOrder(Order order) {
		if ("FoodOrder".equals(order.getName())) {
			System.out.println("cancel food order");
			foodQty++;
		} else if ("DrinkOrder".equals(order.getName())) {
			System.out.println("cancel drink order");
			drinkQty++;
		}
		orderList.remove(order);
	}

	public void sendOrderToKitchen() {
		orderList.forEach(o -> o.sendOrder());
		orderList.clear();
	}

}
