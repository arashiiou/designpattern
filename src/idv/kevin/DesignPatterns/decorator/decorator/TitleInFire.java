package idv.kevin.DesignPatterns.decorator.decorator;

import idv.kevin.DesignPatterns.decorator.component.Adventurer;

public class TitleInFire extends Title {

	public TitleInFire(Adventurer adventurer) {
		super(adventurer);
	}

	@Override
	public void attack() {
		System.out.print("燃燒 ");
		super.attack();
	}

	public void fireBall() {
		System.out.println("丟火球");
	}
}
