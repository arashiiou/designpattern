package idv.kevin.DesignPatterns.strategy3.strategy;

import idv.kevin.DesignPatterns.strategy3.model.Cherry;

public interface Strategy {
	boolean check(Cherry cherry);
}
