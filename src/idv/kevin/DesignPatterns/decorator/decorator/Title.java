package idv.kevin.DesignPatterns.decorator.decorator;

import idv.kevin.DesignPatterns.decorator.component.Adventurer;

public abstract class Title implements Adventurer {

	protected Adventurer adventurer;

	public Title(Adventurer adventurer) {
		this.adventurer = adventurer;
	}

	@Override
	public void attack() {
		adventurer.attack();
	}
}
