package idv.kevin.DesignPatterns.observer;

import org.junit.Test;

import idv.kevin.DesignPatterns.observer.observer.Adventurer;
import idv.kevin.DesignPatterns.observer.observer.Bard;
import idv.kevin.DesignPatterns.observer.observer.Gumman;
import idv.kevin.DesignPatterns.observer.observer.Lancer;
import idv.kevin.DesignPatterns.observer.subject.Association;
import idv.kevin.DesignPatterns.observer.subject.Subject;

public class AssociationTest {
	
	@Test
	public void test() {
		Adventurer lancer = new Lancer("Mick");
		Adventurer lancer2 = new Lancer("Tom");
		Adventurer brad = new Bard("Rom");
		Adventurer gumman = new Gumman("GGman");
		
		Subject association = new Association();
		
		lancer.listen(association);
		lancer2.listen(association);
		brad.listen(association);
		gumman.listen(association);
		
//		association.add(lancer);
//		association.add(lancer2);
//		association.add(brad);
//		association.add(gumman);

		System.out.println("---send easy Q---");
		association.sendQuestions("easy job");
		System.out.println();

		System.out.println("---send difficult Q---");
		association.sendQuestions("hardhardhard job");
		System.out.println();
		
//		association.remove(lancer2);
		lancer2.unListen(association);
		System.out.println("---send difficult 2Q---");
		association.sendQuestions("hardhardhardhardhardhard job");
		
		
		
	}
}
