package idv.kevin.DesignPatterns.template.template;

import idv.kevin.DesignPatterns.template.Adventurer;

public abstract class MazeTemplate {
	protected int difficulty;
	protected Adventurer adventurer;
	protected boolean showSecretMaze;

	public Adventurer adventurer(Adventurer adventurer) {
		this.adventurer = adventurer;
		if (isLevelEnough(adventurer.getLevel())) {
			System.out.println(" -- " + adventurer.getName() + " 進行困難度為'"
					+ difficulty + "'的迷宮");
			createMaze();
			start();

			if (showSecretMaze) {
				intoHiddenMaze();
			}
			showResult();
		}else {
			System.out.println("挑戰者等級太低,無法進入迷宮");
		}
		return this.adventurer;
	}

	void intoHiddenMaze() {
		System.out.println("進入隱藏迷宮");
	}

	Adventurer showResult() {
		this.adventurer.setLevel(this.adventurer.getLevel() + 100);
		System.out.println("完成困難度: " + difficulty + " 的迷宮");
		return this.adventurer;
	}

	private boolean isLevelEnough(int level) {
		if (level < difficulty) {
			return false;
		}
		return true;
	}

	abstract void createMaze();
	abstract void start();
}
