package idv.kevin.DesignPatterns.composite;

import org.junit.Test;

public class BranchOrganizationTest {
	
	@Test
	public void test() {
		Association root  = new Association("總部");
		root.add(new HumanResouce("總部-人力資源處"));
		root.add(new ServiceCenter("總部-服務處"));

		Association northBranch = new Association("北區分部");
		northBranch.add(new HumanResouce("北區分部-人力資源處"));
		northBranch.add(new ServiceCenter("北區分部-服務處"));
		root.add(northBranch);

		Association northPartOneBranch = new Association("北區分部一分處");
		northPartOneBranch.add(new HumanResouce("北區分部一分處-人力資源處"));
		northPartOneBranch.add(new ServiceCenter("北區分部一分處-服務處"));
		northBranch.add(northPartOneBranch);

		Association northPartTwoBranch = new Association("北區分部二分處");
		northPartTwoBranch.add(new HumanResouce("北區分部二分處-人力資源處"));
		northPartTwoBranch.add(new ServiceCenter("北區分部二分處-服務處"));
		northBranch.add(northPartTwoBranch);

		Association southBranch = new Association("南區分部");
		southBranch.add(new HumanResouce("南區分部-人力資源處"));
		southBranch.add(new ServiceCenter("南區分部-服務處"));
		root.add(southBranch);

		Association overseaBranch = new Association("海外分部");
		overseaBranch.add(new HumanResouce("海外分部-人力資源處"));
		overseaBranch.add(new ServiceCenter("海外分部-服務處"));
		root.add(overseaBranch);
		

		System.out.println("結構圖");
		root.display(1);
		System.out.println();
		System.out.println("==================================");
		System.out.println("職責表");
		root.listDuty();;
	}

}
