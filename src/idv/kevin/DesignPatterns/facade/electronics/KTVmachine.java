package idv.kevin.DesignPatterns.facade.electronics;

public class KTVmachine extends Electronic {
	private String song;

	public void selectSong(String song) {
		this.song = song;
	}

	public void playSong() {
		System.out.println(this.getClass().getSimpleName() + "playing " + song);
	}
	
	@Override
	public void showStatus() {
		super.showStatus();
		if(isPowerOn())
		playSong();
	}

}
