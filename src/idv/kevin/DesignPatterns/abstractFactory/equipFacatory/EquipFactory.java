package idv.kevin.DesignPatterns.abstractFactory.equipFacatory;

import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Clothes;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Weapon;

public interface EquipFactory {

	Weapon productWeapon();
	Clothes productClothes();

}
