package idv.kevin.DesignPatterns.factoryPattern.trainingCamp;

import idv.kevin.DesignPatterns.factoryPattern.adventuter.Adventurer;
import idv.kevin.DesignPatterns.factoryPattern.adventuter.Warrior;

public class WarriorTrainingCamp implements TrainingCamp {

	@Override
	public Adventurer trainAdventurer() {
		System.out.println("training a Warrior");
		return new Warrior();
	}

}
