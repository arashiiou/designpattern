package idv.kevin.DesignPatterns.command.receiver;

public class Chef implements KitchenWorker {

	@Override
	public void finishWork() {
		System.out.print("do cooking job");
		System.out.println();
	}

}
