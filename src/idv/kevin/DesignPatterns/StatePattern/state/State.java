package idv.kevin.DesignPatterns.statePattern.state;

public interface State {
	void inserQuarter();

	void ejectQuarter();

	void turnCrank();

	void dispense();
}
