package idv.kevin.DesignPatterns.strategy3.strategy;

import idv.kevin.DesignPatterns.strategy3.model.Cherry;

public class WhiteCherryStrategy implements Strategy{
	@Override
	public boolean check(Cherry cherry) {
		return cherry.getColor().equals("white");
	}
}
