package idv.kevin.DesignPatterns.singletonPattern;

public class SingletonTest extends Thread{
	String myId;
	public SingletonTest(String id) {
		this.myId= id;
	}
	
	
	@Override
	public void run() {
		SingletonGreed instance = SingletonGreed.getInstance();
		if(instance != null)
			System.out.println(myId+" 產生Singleton: "+instance.hashCode());
	}

	public static void main(String[] args) {
		//singel thread
		SingletonGreed s1 = SingletonGreed.getInstance();
		SingletonGreed s2 = SingletonGreed.getInstance();
		System.out.println("s1.hashCode():"+ s1.hashCode());
		System.out.println("s1.hashCode():"+ s2.hashCode());
		
		
		//muti-Thread
		SingletonTest t1 = new SingletonTest("t1");
		SingletonTest t2 = new SingletonTest("t2");
		t1.start();
		t2.start();

	}
}
