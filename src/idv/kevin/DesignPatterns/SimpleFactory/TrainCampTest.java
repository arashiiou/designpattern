package idv.kevin.DesignPatterns.simpleFactory;

import idv.kevin.DesignPatterns.simpleFactory.adventurer.Adventurer;
import idv.kevin.DesignPatterns.simpleFactory.factory.TrainingCampFactory;
import idv.kevin.DesignPatterns.simpleFactory.model.AdventurerEnum;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class TrainCampTest {

	@Test
	public void test() {
		Adventurer adventurer1 = TrainingCampFactory
				.trainAdventurer(AdventurerEnum.ARCHER);
		Assert.assertEquals(adventurer1.getType(), AdventurerEnum.ARCHER.getAdventureType());

		Adventurer adventurer2 = TrainingCampFactory
				.trainAdventurer(AdventurerEnum.WARRIOR);
		Assert.assertEquals(adventurer2.getType(), AdventurerEnum.WARRIOR.getAdventureType());

	}
}
