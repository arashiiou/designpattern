package idv.kevin.DesignPatterns.abstractFactory.trainingCamp;

import idv.kevin.DesignPatterns.abstractFactory.adventurer.Adventurer;

public interface TrainingCamp {

	public Adventurer trainingAdventurer();
}
