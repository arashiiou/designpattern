package idv.kevin.DesignPatterns.facade.electronics;

public class PlayStation3 extends Electronic {
	private String game;

	public void putGameDisk(String game) {
		this.game = game;
	}

	public void startGame() {
		System.out.println(this.getClass().getSimpleName() + " game:" + game
				+ " starting");
	}

	@Override
	public void showStatus() {
		super.showStatus();
		if(isPowerOn())
		System.out.println(
				this.getClass().getSimpleName() + " now is load game " + game);
	}
}
