package idv.kevin.DesignPatterns.decorator;

import org.junit.Test;

import idv.kevin.DesignPatterns.decorator.component.Adventurer;
import idv.kevin.DesignPatterns.decorator.component.Lancer;
import idv.kevin.DesignPatterns.decorator.decorator.TitleAgile;
import idv.kevin.DesignPatterns.decorator.decorator.TitleInFire;
import idv.kevin.DesignPatterns.decorator.decorator.TitleStrong;

public class DecoratorTest {

	@Test
	public void test() {
		Adventurer lancer = new Lancer("Jack");
		System.out.println("---長槍兵 Jack---");
		lancer.attack();

		System.out.println();
		System.out.println("=========取得強壯稱號");
		TitleStrong sJack = new TitleStrong(lancer);
		sJack.attack();

		System.out.println();
		System.out.println("==========取得敏捷稱號");
		TitleAgile aJack = new TitleAgile(sJack);
		aJack.attack();
		aJack.useFlash();

		System.out.println();
		System.out.println("========取得燃燒稱號");
		TitleInFire fJack = new TitleInFire(aJack);
		fJack.attack();
		fJack.fireBall();


		System.out.println();
		System.out.println("再次取得強壯稱號");
		TitleStrong ssJ = new TitleStrong(fJack);
		ssJ.attack();
	}

}
