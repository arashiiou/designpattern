package idv.kevin.DesignPatterns.observer.observer;

public class Gumman extends Adventurer {

	public Gumman(String name) {
		super(name);
	}

	@Override
	public void getQuestions(String questions) {
		if (questions.length() < 10) {
			System.out.println(name
					+ ": it's too easy question, I hava no interest on it");
		} else {
			System.out.println(
					name + ": haha , it's my turn nothing can stop me!");
		}
	}

}
