package idv.kevin.DesignPatterns.simpleFactory.adventurer;

public class Warrior implements Adventurer {

	@Override
	public String getType() {
		System.out.println("I am warrior");
		return this.getClass().getSimpleName();
	}

}
