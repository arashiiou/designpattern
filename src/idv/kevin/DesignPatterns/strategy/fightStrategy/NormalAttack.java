package idv.kevin.DesignPatterns.strategy.fightStrategy;

public class NormalAttack implements FightStrategy {

	@Override
	public void execute() {
		System.out.println("use Normal attack");
	}
}
