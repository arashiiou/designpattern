package idv.kevin.DesignPatterns.command.command;

import idv.kevin.DesignPatterns.command.receiver.KitchenWorker;

public class DrinkOrder extends Order {

	public DrinkOrder(KitchenWorker receiver) {
		super(receiver);
		super.name = this.getClass().getSimpleName();
	}

}
