package idv.kevin.DesignPatterns.statePattern;

import idv.kevin.DesignPatterns.statePattern.state.HasQuarterState;
import idv.kevin.DesignPatterns.statePattern.state.NoQuarterState;
import idv.kevin.DesignPatterns.statePattern.state.SoldOutState;
import idv.kevin.DesignPatterns.statePattern.state.SoldState;
import idv.kevin.DesignPatterns.statePattern.state.State;

public class GumballMachine {
	State soldOutState;
	State noQuarterState;
	State hasQuarterState;
	State soldState;
	State winnerState;

	State state = soldOutState;
	int qty = 0;

	public GumballMachine(int gumballQty) {
		soldOutState = new SoldOutState(this);
		noQuarterState = new NoQuarterState(this);
		hasQuarterState = new HasQuarterState(this);
		soldState = new SoldState(this);
		this.qty = gumballQty;
		if (gumballQty > 0)
			state = noQuarterState;
	}

	public void insertQuarter() {
		state.inserQuarter();
	}

	public void ejectQuarter() {
		state.ejectQuarter();
	}

	public void turnCrank() {
		state.turnCrank();
		state.dispense();
	}

	public void setState(State state) {
		this.state = state;
	}

	public void releaseBall() {
		System.out.println("A gumball comes rolling out the slot");
		if (qty != 0)
			qty -= 1;
	}

	public State getHasQuarterState() {
		return this.hasQuarterState;
	}

	public State getSoldOutState() {
		return this.soldOutState;
	}

	public State getNoQuarterState() {
		return this.noQuarterState;
	}

	public State getSoldState() {
		return this.soldState;
	}

	public State getWinnerState() {
		return this.winnerState;
	}

	public int getQty() {
		return this.qty;
	}

}
