package idv.kevin.DesignPatterns.builder;

import idv.kevin.DesignPatterns.builder.builder.Mac;
import idv.kevin.DesignPatterns.builder.product.MacBookProBuilder;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class BuilderTest {

	@Test
	public void builderTest() {
		MacBookProBuilder macBookProBuilder = new MacBookProBuilder();
		Mac macBookpro = macBookProBuilder.buildMonitorSize(13.3)
			.buildColor("spaceGray")
			.buildStorage(512)
			.buildMemory(16)
			.buildProcessor("i7-3.1G")
			.buildKeyboard("Eng")
			.build();

		Assert.assertEquals("MacBookPro",macBookpro.getModel());
	}

}
