package idv.kevin.DesignPatterns.factoryPattern.adventuter;

public interface Adventurer {
	String getType();
}
