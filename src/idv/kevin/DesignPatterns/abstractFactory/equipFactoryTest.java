package idv.kevin.DesignPatterns.abstractFactory;

import org.junit.Assert;
import org.junit.Test;

import idv.kevin.DesignPatterns.abstractFactory.adventurer.Adventurer;
import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Clothes;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Weapon;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.ArcherEquipFactory;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.EquipFactory;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.WarriorEquipFactory;
import idv.kevin.DesignPatterns.abstractFactory.trainingCamp.ArcherTrainingCamp;
import idv.kevin.DesignPatterns.abstractFactory.trainingCamp.WarriorTrainingCamp;

public class equipFactoryTest {
	private EquipFactory equipFactory;

	@Test
	public void test() {
		System.out.println(" ===== abstract factory test =====");
		equipFactory = new ArcherEquipFactory();
		Weapon archerBow = equipFactory.productWeapon();
		Clothes archerLeather = equipFactory.productClothes();

		Assert.assertEquals(2, archerBow.getAtk());
		Assert.assertEquals(50, archerBow.getRange());
		Assert.assertEquals(10, archerLeather.getDef());

		equipFactory = new WarriorEquipFactory();
		Weapon warriorSword = equipFactory.productWeapon();
		Clothes warriorArmour = equipFactory.productClothes();

		Assert.assertEquals(10, warriorSword.getAtk());
		Assert.assertEquals(30, warriorSword.getRange());
		Assert.assertEquals(20, warriorArmour.getDef());
		
		Adventurer archer = new ArcherTrainingCamp().trainingAdventurer();
		Adventurer warrior = new WarriorTrainingCamp().trainingAdventurer();

		System.out.println();
		
		archer.disPlay();
		System.out.println();
		warrior.disPlay();
		
	}
}
