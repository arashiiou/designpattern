package idv.kevin.DesignPatterns.statePattern.state;

import idv.kevin.DesignPatterns.statePattern.GumballMachine;

public class NoQuarterState implements State {
	GumballMachine gumballMachine;

	public NoQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void inserQuarter() {
		System.out.println("You insert a quarter!");
		gumballMachine.setState(gumballMachine.getHasQuarterState());
	}

	@Override
	public void ejectQuarter() {
		System.out.println("You haven't insert quarter, No quarter can be eject!");
	}

	@Override
	public void turnCrank() {
		System.out.println("You turn, but there is no quarter");
	}

	@Override
	public void dispense() {
		System.out.println("You need to insert quarter first.");
	}

}
