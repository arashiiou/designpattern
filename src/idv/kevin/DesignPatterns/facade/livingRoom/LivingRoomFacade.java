package idv.kevin.DesignPatterns.facade.livingRoom;

import idv.kevin.DesignPatterns.facade.electronics.KTVmachine;
import idv.kevin.DesignPatterns.facade.electronics.PlayStation3;
import idv.kevin.DesignPatterns.facade.electronics.Stereo;
import idv.kevin.DesignPatterns.facade.electronics.Television;
import idv.kevin.DesignPatterns.facade.source.SourceEnum;

public class LivingRoomFacade {
	Television tv = new Television();
	Stereo stereo = new Stereo();
	PlayStation3 ps3 = new PlayStation3();
	KTVmachine ktv = new KTVmachine();

	public void playGame(String game) {
		stereo.powerOn();
		tv.powerOn();
		setSound(50);
		tv.switchSource(SourceEnum.PS);
		ps3.powerOn();
		ps3.putGameDisk(game);
		ps3.startGame();
	}

	public void showTVChannel() {
		tv.showTvChannel();
	}

	public void turnOffAll() {
		stereo.powerOff();
		ktv.powerOff();
		ps3.powerOff();
		tv.powerOff();
	}

	public void watchTV() {
		tv.powerOn();
		tv.switchSource(SourceEnum.TVBOX);
	}

	public void switchTVChannel(int channel) {
		tv.swithcChannel(channel);
	}

	public void useKTV() {
		stereo.powerOn();
		ktv.powerOn();
		tv.powerOn();
		setSound(50);
		tv.switchSource(SourceEnum.KTV);
	}

	public void selectSong(String song) {
		if (ktv.isPowerOn())
			ktv.selectSong(song);
	}

	public void playSong() {
		if (ktv.isPowerOn())
			ktv.playSong();
	}

	public void setSound(int sound) {
		if (tv.isPowerOn()) 
			tv.setSound(sound);
		if (stereo.isPowerOn()) 
			stereo.setSound(sound);
	}
	
	public void showAllStatus() {
		tv.showStatus();
		stereo.showStatus();
		ps3.showStatus();
		ktv.showStatus();
	}
}
