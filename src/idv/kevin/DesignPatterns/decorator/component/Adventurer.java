package idv.kevin.DesignPatterns.decorator.component;

public interface Adventurer {
	void attack();
}
