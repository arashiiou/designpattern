package idv.kevin.DesignPatterns.strategy2.comparator;

import java.util.Comparator;

import idv.kevin.DesignPatterns.strategy2.Village;

public class SortVillageByName implements Comparator<Village> {

	@Override
	public int compare(Village v1, Village v2) {
		if (v1.name.charAt(0) > v2.name.charAt(0))
			return 1;
		if (v1.name.charAt(0) < v2.name.charAt(0))
			return -1;
		return 0;
	}
}
