package idv.kevin.DesignPatterns.strategy2.comparator;

import java.util.Comparator;

import idv.kevin.DesignPatterns.strategy2.Village;

public class SortVillageByPopulation implements Comparator<Village> {

	@Override
	public int compare(Village v1, Village v2) {
		if (v1.population > v2.population)
			return 1;
		if (v1.population < v2.population)
			return -1;
		return 0;
	}
}
