package idv.kevin.DesignPatterns.command.receiver;

public interface KitchenWorker {
	void finishWork();
}
