package idv.kevin.DesignPatterns.abstractFactory.equipFacatory;

import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Clothes;
import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Leather;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Bow;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Weapon;

public class ArcherEquipFactory implements EquipFactory {

	@Override
	public Weapon productWeapon() {
		Weapon weapon = new Bow();
		weapon.setAtk(2);
		weapon.setRange(50);
		return weapon;
	}

	@Override
	public Clothes productClothes() {
		Clothes clothe = new Leather();
		clothe.setDef(10);
		return clothe;
	}

}
