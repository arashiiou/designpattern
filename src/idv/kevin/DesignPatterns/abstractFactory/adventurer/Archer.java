package idv.kevin.DesignPatterns.abstractFactory.adventurer;

public class Archer extends Adventurer {

	@Override
	public void disPlay() {
		System.out.println("I am Archer, and I have");
		weapon.display();
		clothes.display();
	}

}
