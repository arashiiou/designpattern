package idv.kevin.DesignPatterns.abstractFactory.trainingCamp;

import idv.kevin.DesignPatterns.abstractFactory.adventurer.Adventurer;
import idv.kevin.DesignPatterns.abstractFactory.adventurer.Archer;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.ArcherEquipFactory;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.EquipFactory;

public class ArcherTrainingCamp implements TrainingCamp {
	private static EquipFactory equipFactory = new ArcherEquipFactory();

	@Override
	public Adventurer trainingAdventurer() {
		System.out.println("Training an Archer");
		Archer archer = new Archer();
		archer.setWeapon(equipFactory.productWeapon());
		archer.setClothes(equipFactory.productClothes());
		return archer;
	}
}
