package idv.kevin.DesignPatterns.strategy;

import org.junit.Test;

import idv.kevin.DesignPatterns.strategy.fightStrategy.NormalAttack;
import idv.kevin.DesignPatterns.strategy.fightStrategy.UseItem;
import idv.kevin.DesignPatterns.strategy.fightStrategy.UseSkill;

public class FightTest {
	
	@Test
	public void test() {
		Adventurer adventurer = new Adventurer();

		System.out.println("appear Normal monster");
		adventurer.choiceStrategy(new NormalAttack());
		adventurer.attack();
		System.out.println("");

		System.out.println("appear BIG monster");
		adventurer.choiceStrategy(new UseSkill());
		adventurer.attack();
		System.out.println("");

		System.out.println("appear Special monster");
		adventurer.choiceStrategy(new UseItem());
		adventurer.attack();
		System.out.println("");
		
		
		
	}

}
