package idv.kevin.DesignPatterns.abstractFactory.trainingCamp;

import idv.kevin.DesignPatterns.abstractFactory.adventurer.Adventurer;
import idv.kevin.DesignPatterns.abstractFactory.adventurer.Warrior;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.EquipFactory;
import idv.kevin.DesignPatterns.abstractFactory.equipFacatory.WarriorEquipFactory;

public class WarriorTrainingCamp implements TrainingCamp {
	private static EquipFactory equipFactory = new WarriorEquipFactory();

	@Override
	public Adventurer trainingAdventurer() {
		System.out.println("Training a Warrior");
		Warrior warrior = new Warrior();
		warrior.setWeapon(equipFactory.productWeapon());
		warrior.setClothes(equipFactory.productClothes());
		return warrior;
	}
}
