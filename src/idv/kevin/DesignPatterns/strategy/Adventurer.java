package idv.kevin.DesignPatterns.strategy;

import idv.kevin.DesignPatterns.strategy.fightStrategy.FightStrategy;

public class Adventurer {
	FightStrategy fightStrategy ;
	
	public void attack() {
		fightStrategy.execute();
	}
	
	public void choiceStrategy(FightStrategy strategy) {
		this.fightStrategy = strategy;
	}
}
