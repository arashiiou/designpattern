package idv.kevin.DesignPatterns.composite;

import java.util.ArrayList;
import java.util.List;

/*
 * 有分支的協會 Composite
*/
public class Association extends AbstractAssociation {

	private List<AbstractAssociation> branchs = new ArrayList<>();

	public Association() {
	}

	public Association(String name) {
		super(name);
	}

	/*
	 * 增加轄下分會或部門
	 */
	@Override
	public void add(AbstractAssociation association) {
		branchs.add(association);
	}

	/*
	 * 移除轄下分會或部門
	 */
	@Override
	public void remove(AbstractAssociation association) {
		branchs.remove(association);
	}

	/*
	 * 印出組織結構圖
	 */
	@Override
	public void display(int depth) {
		for (int i = 0; i < depth; i++) {
			System.out.print("-");
		}
		System.out.println(name);
		branchs.forEach(a -> {
			a.display(depth + 2);
		});

	}

	/*
	 * 印出組織職責
	 */
	@Override
	public void listDuty() {
		branchs.forEach(a -> {
			a.listDuty();
		});
	}
}
