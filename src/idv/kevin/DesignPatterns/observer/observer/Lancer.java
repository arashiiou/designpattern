package idv.kevin.DesignPatterns.observer.observer;

public class Lancer extends Adventurer {

	public Lancer(String name) {
		super(name);
	}

	@Override
	public void getQuestions(String questions) {
		System.out.println(name+": no matter what kind of the question, I always catch it !");
	}

}
