package idv.kevin.DesignPatterns.facade.electronics;

public class Stereo extends Electronic {
	private int sound = 50;

	public void setSound(int sound) {
		this.sound = sound;
	}

	public int getShoud() {
		return this.sound;
	}

	@Override
	public void showStatus() {
		super.showStatus();
		if (isPowerOn()) {
			System.out.println("音量為:" + sound);
		}
	}
}
