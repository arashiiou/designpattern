package idv.kevin.DesignPatterns.observer.subject;

public class Association extends Subject {

	@Override
	public void sendQuestions(String questions) {
		observers.forEach(a->{
			a.getQuestions(questions);
		});
	}

}
