package idv.kevin.DesignPatterns.command.command;

import idv.kevin.DesignPatterns.command.receiver.KitchenWorker;

public class FoodOrder extends Order {

	public FoodOrder(KitchenWorker receiver) {
		super(receiver);
		super.name = this.getClass().getSimpleName();
	}

}
