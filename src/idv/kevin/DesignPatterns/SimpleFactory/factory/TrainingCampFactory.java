package idv.kevin.DesignPatterns.simpleFactory.factory;

import idv.kevin.DesignPatterns.simpleFactory.adventurer.Adventurer;
import idv.kevin.DesignPatterns.simpleFactory.adventurer.Archer;
import idv.kevin.DesignPatterns.simpleFactory.adventurer.Warrior;
import idv.kevin.DesignPatterns.simpleFactory.model.AdventurerEnum;

public class TrainingCampFactory {
	public static Adventurer trainAdventurer(AdventurerEnum adventurerType) {
		Adventurer adventurer;

		switch (adventurerType) {
			case ARCHER: {
				System.out.println("Training a Archer");
				adventurer = new Archer();
				break;
			}
			case WARRIOR: {
				System.out.println("Training a Warrior");
				adventurer = new Warrior();
				break;
			}
			default: {
				adventurer = null;
			}
		}
		return adventurer;
	}
}
