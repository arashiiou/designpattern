package idv.kevin.DesignPatterns.builder.product;

import idv.kevin.DesignPatterns.builder.builder.Mac;

public class MacBookProBuilder implements MacBookBuilder {

	private Mac mac;

	public MacBookProBuilder() {
		mac = new Mac();
		mac.setModel("MacBookPro");
	}

	@Override
	public MacBookBuilder buildMonitorSize(Double monitorSize) {
		mac.setMonitorSize(monitorSize);
		return this;
	}

	@Override
	public MacBookBuilder buildColor(String color) {
		mac.setColor(color);
		return this;
	}

	@Override
	public MacBookBuilder buildStorage(Integer storage) {
		mac.setStorage(storage);
		return this;
	}

	@Override
	public MacBookBuilder buildMemory(Integer memory) {
		mac.setMemory(memory);
		return this;
	}

	@Override
	public MacBookBuilder buildProcessor(String processor) {
		mac.setProcessor(processor);
		return this;
	}

	@Override
	public MacBookBuilder buildKeyboard(String keyboard) {
		mac.setKeyboard(keyboard);
		return this;
	}

	@Override
	public Mac build() {
		return mac;
	}
}
