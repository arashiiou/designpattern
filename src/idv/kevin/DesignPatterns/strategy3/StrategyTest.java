package idv.kevin.DesignPatterns.strategy3;

import idv.kevin.DesignPatterns.strategy3.strategy.RedCherryStrategy;
import idv.kevin.DesignPatterns.strategy3.strategy.Strategy;
import idv.kevin.DesignPatterns.strategy3.model.Cherry;

import java.util.ArrayList;
import java.util.List;

public class StrategyTest {

	public static void main(String[] args) {

		List<Cherry> cherries = getApples();

		// 一般解法
		List<Cherry> redCherries = filterCherryByStrategy(cherries, new RedCherryStrategy());

		// 內部類別
		List<Cherry> whiteCherries = filterCherryByStrategy(cherries, new Strategy() {
			@Override public boolean check(Cherry apple) {
				return apple.getColor().equals("green");
			}
		});

		// Lambda
		List<Cherry> $9rowCherries = filterCherryByStrategy(cherries, cherry -> cherry.getSize() >= 9);

	}

	public static List<Cherry> filterCherryByStrategy(List<Cherry> cherries, Strategy strategy){

		ArrayList<Cherry> result = new ArrayList<>();

		cherries.forEach(cherry -> {
			if (strategy.check(cherry)) {
				result.add(cherry);
			}
		});

		return result;
	}

	public static List<Cherry> getApples(){

		ArrayList<Cherry> cherries = new ArrayList<>();
		cherries.add(generateCherry(1,"red",10));
		cherries.add(generateCherry(2,"green",20));
		cherries.add(generateCherry(3,"red",21));
		cherries.add(generateCherry(4,"green",9));

		return cherries;
	}

	public static Cherry generateCherry(Integer id,String color,double weight){

		Cherry cherry = new Cherry();
		cherry.setId(id);
		cherry.setColor(color);
		cherry.setSize(weight);

		return cherry;
	}
}
