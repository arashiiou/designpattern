package idv.kevin.DesignPatterns.template.template;

public class HardMaze extends MazeTemplate {
	public HardMaze() {
		super.showSecretMaze = true;
		super.difficulty= 99;
	}

	@Override
	void createMaze() {
		System.out.println("level:99 different maze created!!");
		System.out.println("the maze has 99x99 size");
	}

	@Override
	void start() {
		System.out.println("挑戰者進行困難迷宮～");
	}

}
