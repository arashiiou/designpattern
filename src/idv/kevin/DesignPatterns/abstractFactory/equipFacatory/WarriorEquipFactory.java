package idv.kevin.DesignPatterns.abstractFactory.equipFacatory;

import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Armour;
import idv.kevin.DesignPatterns.abstractFactory.equip.clothes.Clothes;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.LongSword;
import idv.kevin.DesignPatterns.abstractFactory.equip.weapon.Weapon;

public class WarriorEquipFactory implements EquipFactory {

	@Override
	public Weapon productWeapon() {
		LongSword weapon = new LongSword();
		weapon.setAtk(10);
		weapon.setRange(30);
		return weapon;
	}

	@Override
	public Clothes productClothes() {
		Armour clothe = new Armour();
		clothe.setDef(20);
		return clothe;
	}

}
