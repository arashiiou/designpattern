package idv.kevin.DesignPatterns.builder.product;

import idv.kevin.DesignPatterns.builder.builder.Mac;

public interface MacBookBuilder {
	MacBookBuilder buildMonitorSize(Double monitorSize);
	MacBookBuilder buildColor(String color);
	MacBookBuilder buildStorage(Integer storage);
	MacBookBuilder buildMemory(Integer memory);
	MacBookBuilder buildProcessor(String processor);
	MacBookBuilder buildKeyboard(String keyboard);
	Mac build();
}
