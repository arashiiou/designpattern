package idv.kevin.DesignPatterns.factoryPattern.trainingCamp;

import idv.kevin.DesignPatterns.factoryPattern.adventuter.Adventurer;
import idv.kevin.DesignPatterns.factoryPattern.adventuter.Archer;

public class ArcherTrainingCamp implements TrainingCamp {

	@Override
	public Adventurer trainAdventurer() {
		System.out.println("Training an Archer");
		return new Archer();
	}

}
