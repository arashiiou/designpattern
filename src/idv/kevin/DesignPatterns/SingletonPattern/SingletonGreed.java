package idv.kevin.DesignPatterns.singletonPattern;

public class SingletonGreed {
	private static SingletonGreed instance ;
	
	private SingletonGreed() {};
	
	public static  SingletonGreed getInstance() {
		synchronized(SingletonGreed.class) {
			if(instance == null) {
				instance = new SingletonGreed();
			}
			return instance;
		}
	}

}
