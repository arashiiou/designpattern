package idv.kevin.DesignPatterns.strategy2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import idv.kevin.DesignPatterns.strategy2.comparator.SortVillageByArea;
import idv.kevin.DesignPatterns.strategy2.comparator.SortVillageById;
import idv.kevin.DesignPatterns.strategy2.comparator.SortVillageByName;
import idv.kevin.DesignPatterns.strategy2.comparator.SortVillageByPopulation;

public class StrategyTest {

	@Test
	public void test() {
		Village appleFatm = new Village(3, "apple farm", 32, 5.1);
		Village capeTown = new Village(1, "cape town", 45, 2.3);
		Village stethValley = new Village(4, "steth valley", 22, 6.7);
		Village barnField = new Village(2, "barn field", 52, 4.9);

		List<Village> villages = new ArrayList<>();
		villages.add(appleFatm);
		villages.add(capeTown);
		villages.add(stethValley);
		villages.add(barnField);

		System.out.println("Non sort");
		showList(villages);

		System.out.println("Sort by id");
		Collections.sort(villages, new SortVillageById());
		showList(villages);

		System.out.println("Sort by name");
		Collections.sort(villages, new SortVillageByName());
		showList(villages);

		System.out.println("Sort by area");
		Collections.sort(villages, new SortVillageByArea());
		showList(villages);

		System.out.println("Sort by population");
		Collections.sort(villages, new SortVillageByPopulation());
		showList(villages);
	}

	void showList(List<Village> villages) {
		villages.forEach(System.out::println);
	}
}
