package idv.kevin.DesignPatterns.statePattern.state;

import java.util.Random;

import idv.kevin.DesignPatterns.statePattern.GumballMachine;

public class HasQuarterState implements State {
	Random randomWinner = new Random(System.currentTimeMillis());
	GumballMachine gumballMachine;

	public HasQuarterState(GumballMachine gumballMachine) {
		this.gumballMachine = gumballMachine;
	}

	@Override
	public void inserQuarter() {
		System.out.println("You already insert quarter, can't insert another quarter");
	}

	@Override
	public void ejectQuarter() {
		System.out.println("Quarter returned!");
		gumballMachine.setState(gumballMachine.getNoQuarterState());
	}

	@Override
	public void turnCrank() {
		System.out.println("You turned...");
		int winner = randomWinner.nextInt(10);
		if((winner==0)&&gumballMachine.getQty()>1 ) {
			gumballMachine.setState(gumballMachine.getWinnerState());
		}else {
			gumballMachine.setState(gumballMachine.getSoldState());
		}
	}

	@Override
	public void dispense() {
		System.out.println("No gumball dispensed");
	}

}
