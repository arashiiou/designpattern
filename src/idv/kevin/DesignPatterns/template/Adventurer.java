package idv.kevin.DesignPatterns.template;

public abstract class Adventurer {
	protected int level;
	protected String name;
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
