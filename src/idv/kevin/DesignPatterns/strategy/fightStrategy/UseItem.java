package idv.kevin.DesignPatterns.strategy.fightStrategy;

public class UseItem implements FightStrategy {

	@Override
	public void execute() {
		System.out.println("use Item attack");
	}
}
