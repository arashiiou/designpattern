package idv.kevin.DesignPatterns.observer.observer;

public class Bard extends Adventurer {

	public Bard(String name) {
		super(name);
	}

	@Override
	public void getQuestions(String questions) {
		if (questions.length() > 10) {
			System.out.println(name
					+ ": too difficult question, I do not want to catch it !");
		} else {
			System.out.println(
					name + " : well, it's a easy question, I can catch it !");
		}
	}

}
