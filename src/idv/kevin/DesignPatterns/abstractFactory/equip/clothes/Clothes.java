package idv.kevin.DesignPatterns.abstractFactory.equip.clothes;

public abstract class Clothes {
	protected int def; // 防禦
	public void display() {
		System.out.println(this.getClass().getSimpleName() + ": def = " + def);
	}
	public int getDef() {
		return def;
	}
	public void setDef(int def) {
		this.def = def;
	}
}
